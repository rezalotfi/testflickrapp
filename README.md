Flickr App for Interview Test
=


## Architecture

The architecure for this project is Clean Architecture with MVVM using Android Lifecycle ViewModel.



<img src="https://i.imgur.com/u63VEpH.png " width="70%" align="middle">

#### Some Key Points on the architecture
- Clean Architecture: Decoupled and using interfaces to allow testing, maintainence and develop presentation layer and data layer without changing domain layer.
- MVVM: Utilizing Jetpack Lifecycle we can have ViewModels that behave as an overseer of the views and shares a lifecycle with the application. One benefit of this is data isn't lost on screen rotation.
- Modular: The app is structured in a way that common functionality is in a library module which can be utilized across projects. Such as networking setup. For every new feature we create a new model and add it to the app. One of the nice benefits of this include faster build times but also supports instant apps and [dynamic features](https://developer.android.com/studio/projects/dynamic-delivery)
- Kotlin First: This project is Kotlin based so we prioritize libraries that are made for Kotlin like [koin](https://github.com/InsertKoinIO/koin).


## Libraries and Frameworks

### Testing
- [Mockito Kotlin](https://github.com/nhaarman/mockito-kotlin)
- [JUnit](https://github.com/junit-team/junit4)

### Networking
- [Retrofit](https://square.github.io/retrofit/) 
- [OkHttp](https://square.github.io/okhttp/)
- [Moshi](https://github.com/square/retrofit/tree/master/retrofit-converters/moshi) [Moshi Retrofit Convertor only]

### Code Structure
- [RxJava](https://github.com/ReactiveX/RxJava) [Reactive programming]
- [koin ](https://github.com/InsertKoinIO/koin) [Kotlin Dependency Injection framework]
- [RxPaper](https://github.com/pakoito/RxPaper) [Rx Wrapper for Paper a NoSQL DB. Used for caching.]

### Code Style and Tools
- [ktlint (Kotlin Style Guide Static Code Analysis Tool)](https://github.com/shyiko/ktlint) [Kotlin Style Guide]
- [detekt (Kotlin Static Code Analysis Tool)](https://github.com/arturbosch/detekt)

### UI
- [Glide](https://github.com/bumptech/glide)

### General
- [Jetpack- Lifecycle components](https://developer.android.com/jetpack/docs/getting-started/)



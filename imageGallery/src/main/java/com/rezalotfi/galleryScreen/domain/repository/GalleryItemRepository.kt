package com.rezalotfi.galleryScreen.domain.repository

import com.rezalotfi.galleryScreen.domain.model.GalleryItem
import io.reactivex.Single

interface GalleryItemRepository {

    fun get(refresh: Boolean): Single<List<GalleryItem>>
}
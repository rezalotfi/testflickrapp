package com.rezalotfi.galleryScreen.domain.usecase

import com.rezalotfi.galleryScreen.domain.model.GalleryItem
import com.rezalotfi.galleryScreen.domain.repository.GalleryItemRepository
import io.reactivex.Single

class GalleryItemUseCase constructor(private val galleryItemRepository: GalleryItemRepository) {

    fun get(refresh: Boolean): Single<List<GalleryItem>> =
        galleryItemRepository.get(refresh)
}
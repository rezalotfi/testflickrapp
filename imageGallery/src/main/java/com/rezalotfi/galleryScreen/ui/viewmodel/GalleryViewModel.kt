package com.rezalotfi.galleryScreen.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rezalotfi.galleryScreen.domain.usecase.GalleryItemUseCase
import com.rezalotfi.galleryScreen.ui.model.GalleryListItem
import com.rezalotfi.galleryScreen.ui.model.mapToUIModel
import com.rezalotfi.presentation.Resource
import com.rezalotfi.presentation.setError
import com.rezalotfi.presentation.setLoading
import com.rezalotfi.presentation.setSuccess
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class GalleryViewModel constructor(private val galleryItemUseCase: GalleryItemUseCase) :
    ViewModel() {
    val galleryItems = MutableLiveData<Resource<List<GalleryListItem>>>()
    private val compositeDisposable = CompositeDisposable()

    fun get(refresh: Boolean) =
        compositeDisposable.add(galleryItemUseCase.get(refresh)
            .doOnSubscribe { galleryItems.setLoading() }
            .subscribeOn(Schedulers.io())
            .map { it.mapToUIModel() }
            .subscribe({ galleryItems.setSuccess(it) }, { galleryItems.setError(it.message) })
        )

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }
}

package com.rezalotfi.galleryScreen.ui.views

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.rezalotfi.galleryScreen.R
import com.rezalotfi.galleryScreen.injectFeature
import com.rezalotfi.galleryScreen.ui.model.GalleryListItem
import com.rezalotfi.galleryScreen.ui.viewmodel.GalleryViewModel
import com.rezalotfi.galleryScreen.utils.DateUtils
import com.rezalotfi.navigation.features.AppMainNavigation
import com.rezalotfi.presentation.Resource
import com.rezalotfi.presentation.ResourceState
import com.rezalotfi.presentation.startRefreshing
import com.rezalotfi.presentation.stopRefreshing
import kotlinx.android.synthetic.main.activity_gallery.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class GalleryScreenActivity : AppCompatActivity() {

    private val vm: GalleryViewModel by viewModel()
    private val adapter by lazy { GalleryItemListAdapter(itemClick) }
    private val snackBar by lazy {
        Snackbar.make(swipeRefreshLayout, getString(R.string.error), Snackbar.LENGTH_INDEFINITE)
            .setAction(getString(R.string.retry)) { vm.get(refresh = true) }
    }

    private val itemClick: (GalleryListItem) -> Unit =
        { startActivity(AppMainNavigation.galleryDetails(it.author, it.itemName, it.datePublished)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)

        injectFeature()

        window?.apply {
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            statusBarColor = ContextCompat.getColor(applicationContext,android.R.color.transparent)
            navigationBarColor = ContextCompat.getColor(applicationContext,android.R.color.transparent)
            val bg = ContextCompat.getDrawable(applicationContext,R.drawable.bg_statusbar)
            window.setBackgroundDrawable(bg)
        }

        if (savedInstanceState == null)
            vm.get(true)

        initData()
    }

    private fun initData(){
        vm.galleryItems.observe(this, { updateGalleryItems(it) })
        galleriesRecyclerView.adapter = adapter
        swipeRefreshLayout.setOnRefreshListener { vm.get(refresh = true) }

        txtGreeting.text = DateUtils.getTimeGreetingMessage()
    }


    private fun updateGalleryItems(resource: Resource<List<GalleryListItem>>?) {
        resource?.let {
            when (it.state) {
                ResourceState.LOADING -> swipeRefreshLayout.startRefreshing()
                ResourceState.SUCCESS -> swipeRefreshLayout.stopRefreshing()
                ResourceState.ERROR -> swipeRefreshLayout.stopRefreshing()
            }
            it.data?.let { adapter.submitList(it) }
            it.message?.let { snackBar.show() }
        }
    }
}
package com.rezalotfi.galleryScreen.data.repository

import com.rezalotfi.galleryScreen.data.datasource.GalleryItemCacheDataSource
import com.rezalotfi.galleryScreen.data.datasource.GalleryItemRemoteDataSource
import com.rezalotfi.galleryScreen.domain.model.GalleryItem
import com.rezalotfi.galleryScreen.domain.repository.GalleryItemRepository
import io.reactivex.Single

class GalleryItemRepositoryImpl constructor(
    private val cacheDataSource: GalleryItemCacheDataSource,
    private val remoteDataSource: GalleryItemRemoteDataSource
) : GalleryItemRepository {

    override fun get(refresh: Boolean): Single<List<GalleryItem>> =
        when (refresh) {
            true -> remoteDataSource.get()
                .flatMap { cacheDataSource.set(it) }
            false -> cacheDataSource.get()
                .onErrorResumeNext { get(true) }
        }
}

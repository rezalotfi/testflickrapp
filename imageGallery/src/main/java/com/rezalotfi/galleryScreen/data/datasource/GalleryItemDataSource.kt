package com.rezalotfi.galleryScreen.data.datasource

import com.rezalotfi.galleryScreen.domain.model.GalleryItem
import io.reactivex.Single

interface GalleryItemCacheDataSource {

    fun get(): Single<List<GalleryItem>>

    fun set(list: List<GalleryItem>): Single<List<GalleryItem>>
}

interface GalleryItemRemoteDataSource {

    fun get(): Single<List<GalleryItem>>
}

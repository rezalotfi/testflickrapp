package com.rezalotfi.galleryScreen

import com.rezalotfi.cache.ReactiveCache
import com.rezalotfi.galleryScreen.data.datasource.GalleryItemCacheDataSource
import com.rezalotfi.galleryScreen.data.datasource.GalleryItemRemoteDataSource
import com.rezalotfi.galleryScreen.data.repository.GalleryItemRepositoryImpl
import com.rezalotfi.galleryScreen.datasource.cache.GalleryItemCacheDataSourceImpl
import com.rezalotfi.galleryScreen.datasource.model.GalleryItemEntity
import com.rezalotfi.galleryScreen.datasource.remote.GalleryItemApi
import com.rezalotfi.galleryScreen.datasource.remote.GalleryItemRemoteDataSourceImpl
import com.rezalotfi.galleryScreen.domain.repository.GalleryItemRepository
import com.rezalotfi.galleryScreen.domain.usecase.GalleryItemUseCase
import com.rezalotfi.galleryScreen.ui.viewmodel.GalleryViewModel
import com.rezalotfi.network.createNetworkClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.module.Module
import org.koin.core.qualifier.qualifier
import org.koin.dsl.module
import retrofit2.Retrofit

fun injectFeature() = loadFeature

private val loadFeature by lazy {
    loadKoinModules(
        listOf(
            useCaseModule,
            viewModelModule,
            repositoryModule,
            dataSourceModule,
            networkModule,
            cacheModule
        )
    )
}

private const val BASE_URL = "https://www.flickr.com/"

private val retrofit: Retrofit = createNetworkClient(BASE_URL, BuildConfig.DEBUG)

val useCaseModule: Module = module {
    factory { GalleryItemUseCase(galleryItemRepository = get()) }
}

val viewModelModule: Module = module {
    viewModel { GalleryViewModel(galleryItemUseCase = get()) }
}

val repositoryModule: Module = module {
    single {
        GalleryItemRepositoryImpl(
            cacheDataSource = get(),
            remoteDataSource = get()
        ) as GalleryItemRepository
    }
}

val dataSourceModule: Module = module {
    single { GalleryItemRemoteDataSourceImpl(api = galleryItemApi) as GalleryItemRemoteDataSource }
    single { GalleryItemCacheDataSourceImpl(cache = get(qualifier(GALLERYITEM_CACHE) )) as GalleryItemCacheDataSource }
}

val networkModule: Module = module {
    single { galleryItemApi }
}

val cacheModule: Module = module {
    single(qualifier(GALLERYITEM_CACHE)) { ReactiveCache<List<GalleryItemEntity>>() }
}

private val galleryItemApi: GalleryItemApi = retrofit.create(GalleryItemApi::class.java)
private const val GALLERYITEM_CACHE = "GALLERYITEM_CACHE"
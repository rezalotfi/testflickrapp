package com.rezalotfi.galleryScreen.datasource.remote

import com.rezalotfi.galleryScreen.data.datasource.GalleryItemRemoteDataSource
import com.rezalotfi.galleryScreen.datasource.model.mapToDomain
import com.rezalotfi.galleryScreen.domain.model.GalleryItem
import io.reactivex.Single

class GalleryItemRemoteDataSourceImpl constructor(
    private val api: GalleryItemApi
) : GalleryItemRemoteDataSource {

    override fun get(): Single<List<GalleryItem>> =
        api.getGalleryItems("json")
            .map { it.items.mapToDomain() }
}

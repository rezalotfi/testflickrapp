package com.rezalotfi.galleryScreen.datasource.cache

import com.rezalotfi.cache.ReactiveCache
import com.rezalotfi.galleryScreen.data.datasource.GalleryItemCacheDataSource
import com.rezalotfi.galleryScreen.domain.model.GalleryItem
import io.reactivex.Single

class GalleryItemCacheDataSourceImpl constructor(
    private val cache: ReactiveCache<List<GalleryItem>>
) : GalleryItemCacheDataSource {

    val key = "GalleryItem List"

    override fun get(): Single<List<GalleryItem>> =
        cache.load(key)

    override fun set(list: List<GalleryItem>): Single<List<GalleryItem>> =
        cache.save(key, list)
}

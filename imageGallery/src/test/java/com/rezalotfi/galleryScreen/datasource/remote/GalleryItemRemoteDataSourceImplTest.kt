@file:Suppress("IllegalIdentifier")

package com.rezalotfi.galleryScreen.datasource.remote

import com.rezalotfi.galleryScreen.datasource.model.mapToDomain
import com.rezalotfi.galleryScreen.galleryItemEntity
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.rezalotfi.galleryScreen.datasource.model.ResponseItem
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class GalleryItemRemoteDataSourceImplTest {

    private lateinit var dataSource: GalleryItemRemoteDataSourceImpl

    private val mockApi: GalleryItemApi = mock()

    private val remoteItem = galleryItemEntity.copy()

    private val remoteList = listOf(remoteItem)

    private val throwable = Throwable()

    private val formatQueryParam = "json"

    @Before
    fun setUp() {
        dataSource = GalleryItemRemoteDataSourceImpl(mockApi)
    }

    @Test
    fun `get comments remote success`() {
        // given
        whenever(mockApi.getGalleryItems(formatQueryParam)).thenReturn(
            Single.just(
                ResponseItem(
                    remoteItem.title,
                    remoteItem.link,
                    remoteItem.description,
                    remoteItem.dateTaken,
                    remoteItem.author,
                    remoteList
                )
            )
        )

        // when
        val test = dataSource.get().test()

        // then
        verify(mockApi).getGalleryItems(formatQueryParam)
        test.assertValue(remoteList.mapToDomain())
    }

    @Test
    fun `get comments remote fail`() {
        // given
        whenever(mockApi.getGalleryItems(formatQueryParam)).thenReturn(Single.error(throwable))

        // when
        val test = dataSource.get().test()

        // then
        verify(mockApi).getGalleryItems(formatQueryParam)
        test.assertError(throwable)
    }
}

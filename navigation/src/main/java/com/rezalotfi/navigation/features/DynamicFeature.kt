package com.rezalotfi.navigation.features

interface DynamicFeature<T> {
    val dynamicStart: T?
}
